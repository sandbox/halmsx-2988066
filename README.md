# bimserver.7.x

This module is developed with the following  
BIMServer version: 1.5.96  
JAVA version: Java(TM) SE Runtime Environment (build 1.8.0_171-b11)  
Platform: MacOS High Sierra 10.13.x  
Drupal: 7.59  

# Why still Drupal 7?
I built this module to work with my existing D7 project. It is working and for now I am sticking to this. Once my project is 
completed, I will try to find the time to port it to D8.

#What is BIM?
If you have to ask, this link provides an introduction: https://en.wikipedia.org/wiki/Building_information_modeling