<?php
function bimserver_links () {
    $html = '';

    if ((variable_get('bimserver_address') !== null) && (variable_get('bimserver_port') !== null) && (variable_get('bimserver_path') !== null)) {
        $bimserver_links_form = drupal_get_form('bimserver_links_form');

        $html .= drupal_render($bimserver_links_form);
    }

    return $html;
}

function bimserver_links_form ($form, &$form_state)
{
    $form['bimserver'] = array(
        '#type' => 'fieldset',
        '#title' => t('BIMServer web modules (links)'),
        '#collapsible' => false,
        '#description' => t('These are just links to default modules provided by your BIMServer.'),
    );

    $bimserver_address = variable_get('bimserver_address');
    $bimserver_port = variable_get('bimserver_port');
    $bimserver_path = variable_get('bimserver_path');

    // l options
    $options = array('attributes' => array('target' => '_blank'));
    
    $form['bimserver']['bimserver'] = array(
        '#type' => 'markup',
        '#markup' => '<p>'. l('BIMserver default page', "http://$bimserver_address:$bimserver_port", $options) . '</p>',
    );

    $form['bimserver']['bimserverjavascriptapi'] = array(
        '#type' => 'markup',
        '#markup' => '<p>'. l('BIMserver JavaScript API', "http://$bimserver_address:$bimserver_port/$bimserver_path/bimserverjavascriptapi", $options) . '</p>',
    );
    
    $form['bimserver']['bimviews'] = array(
        '#type' => 'markup',
        '#markup' => '<p>'. l('BIMvie.ws', "http://$bimserver_address:$bimserver_port/$bimserver_path/bimviews", $options) . '</p>',
    );
    
    $form['bimserver']['console'] = array(
        '#type' => 'markup',
        '#markup' => '<p>'. l('Console', "http://$bimserver_address:$bimserver_port/$bimserver_path/console", $options) . '</p>',
    );

    return $form;
}