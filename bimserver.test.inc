<?php
/* CONNECTION TESTS */
function bimserver_test()
{
    $html = '';

    $bimserver_status_form = drupal_get_form('bimserver_status_form');
    $bimserver_login_test_form = drupal_get_form('bimserver_login_test_form');

    $html .= drupal_render($bimserver_status_form);
    $html .= drupal_render($bimserver_login_test_form);

    return $html;
}

function bimserver_login_test_form ($form, &$form_state)
{
    $form['bimserver'] = array(
        '#type' => 'fieldset',
        '#title' => t('BIMServer login test'),
        // '#weight' => 5,
        '#collapsible' => false,
    );
    
    $form['bimserver']['message'] = array(
        '#type' => 'markup',
        '#markup' => t('To be implemented'),
    );
    
    return $form;
}

function bimserver_status_form($form, &$form_state)
{
    $form['bimserver'] = array(
        '#type' => 'fieldset',
        '#title' => t('BIMServer live information status'),
        // '#weight' => 5,
        '#collapsible' => false,
    );

    if ((variable_get('bimserver_address') !== null) && (variable_get('bimserver_port') !== null) && (variable_get('bimserver_path') !== null)) {
        $html = '';
        
        $address = variable_get('bimserver_address');
        $port = variable_get('bimserver_port');
        $path = variable_get('bimserver_path');

        $uri = 'http://' . $address . ':' . $port . '/json';

        $options = array(
            'method' => 'POST',
            'data' => '{
                "request": {
                  "interface": "AdminInterface", 
                  "method": "getServerInfo", 
                  "parameters": {
                  }
                }
              }',
            'timeout' => 5,
            'headers' => array('Content-Type' => 'application/json')
        );
        
        $response = drupal_http_request($uri, $options);

        // var_dump($response->code);

        if ($response->code > 100) {
            $data = json_decode($response->data);
    
            $errorMessage = $data->response->result->errorMessage;
            $serverState = $data->response->result->serverState;
            $version = $data->response->result->version->fullString;
            $supportEmail = $data->response->result->version->supportEmail;
            $supportUrl = $data->response->result->version->supportUrl;
    
            $html .= $response->data;
            
            if ($errorMessage !== null) {
                $form['bimserver']['errorMessage'] = array(
                    '#type' => 'item',
                    '#title' => t('Error message'),
                    '#markup' => $errorMessage,
                );
            }
            
            $form['bimserver']['serverState'] = array(
                '#type' => 'item',
                '#title' => t('Server state'),
                '#markup' => $serverState,
            );
            
            $form['bimserver']['version'] = array(
                '#type' => 'item',
                '#title' => t('BIMServer version'),
                '#markup' => $version,
            );
            
            $form['bimserver']['supportEmail'] = array(
                '#type' => 'item',
                '#title' => t('Support email'),
                '#markup' => $supportEmail,
            );
            
            $form['bimserver']['supportUrl'] = array(
                '#type' => 'item',
                '#title' => t('Support URL'),
                '#markup' => $supportUrl,
            );
        } else {
            $html .= $response->error;
        }
        
        $form['bimserver']['server_response'] = array(
            '#type' => 'textarea',
            '#title' => t('Server response'),
            '#value' => $html,
            '#cols' => 60,
            '#resizable' => false,
        );
        
        $form['bimserver']['test'] = array(
            '#type' => 'submit',
            '#title' => t('BIMServer IP/URL address'),
            '#default_value' => t('Test again'),
            '#required' => true,
        );
    } else {
        $form['bimserver']['supportUrl'] = array(
            '#type' => 'markup',
            // '#title' => t('Support URL'),
            '#markup' => 'BIMServer not configured',
        );
    }

    return $form;
}
